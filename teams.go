package clogger

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

// Teams groups based on error type
var teamsGroups Groups

// SetTeams adds a teams channel to the logger
func SetTeams(groups Groups) {
	teamsGroups = groups
}

// AddTeam adds a team to the group
func AddTeam(logType int, webhook string) {
	// Init teams map
	if teamsGroups == nil {
		teamsGroups = make(Groups)

		//Init each key type
		teamsGroups[LOG] = nil
		teamsGroups[WARNING] = nil
		teamsGroups[ERROR] = nil
		teamsGroups[FATAL] = nil
	}

	// Add the team to the map
	teamsGroups[logType] = append(teamsGroups[logType], webhook)
}

// teamsWriter send the message to the teams webhooks
func teamsWrite(logType int, msg string) {
	if teamsGroups == nil {
		return // Teams not init
	}

	// Get http client
	client := &http.Client{}

	// Loop over the teams and send the message
	for _, webhook := range teamsGroups[logType] {
		req, err := http.NewRequest("POST", webhook, bytes.NewBuffer([]byte(`{"text":"`+msg+`"}`)))
		resp, err := client.Do(req)
		if err != nil {
			log.Println(fmt.Sprintf("(error) There was an error sending log to teams: %s", err))
			continue
		}
		defer resp.Body.Close()

		// Read the body
		body, _ := ioutil.ReadAll(resp.Body)
		if string(body) != "1" {
			log.Println("(error) Something went wrong while sending message to team: %s", webhook)
		}
	}
}
