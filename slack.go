package clogger

import (
	"fmt"
	"log"

	"github.com/slack-go/slack"
)

// Setup the slack client
var slackClient *slack.Client

// Slack groups based on error type
var slackGroups Groups

// SetSlack inits the slack client with an api key and groups
func SetSlack(key string, groups Groups) {
	slackClient = slack.New(key)
	slackGroups = groups
}

func slackWrite(logType int, msg string) {
	if slackGroups == nil {
		return //Slack not init
	}

	for _, slackGroup := range slackGroups[logType] {
		_, _, err := slackClient.PostMessage(slackGroup, slack.MsgOptionText(msg, true))
		if err != nil {
			log.Println(fmt.Sprintf("(error) There was an error sending log to slack: %s", err))
			return
		}
	}
}
