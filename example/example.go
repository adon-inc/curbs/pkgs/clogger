package main

import (
	"fmt"
	"time"

	"gitlab.com/adon-inc/curbs/pkgs/clogger"
)

func main() {
	var env string = "development"
	var filePath = "./"

	// Initialize the logger
	err := clogger.Init(env, filePath)
	if err != nil {
		fmt.Printf("Error occured while loading clogger: %s\n", err)
	}
	defer clogger.Close()

	// Setup slack support
	// clogger.SetSlack("api-key", clogger.Groups{
	// 	clogger.LOG: []string{"groupid"},
	// })

	// Setup teams support
	// clogger.SetTeams(clogger.Groups{
	// 	clogger.LOG: []string{"webhook"},
	// })

	// Standard log - used for general logging and notes
	clogger.Log("just wanted to log something at this point")
	clogger.Log("just wanted to log something at this point: %v", 123)

	// Warning log - used to denote if something didnt work right but didnt quite break anything
	clogger.Warn("something isnt quite right here")
	clogger.Warn("something isnt quite right here: %v", 123)

	// Error log - used to note that something is wrong in the action
	clogger.Error("something went wrong here")
	clogger.Error("something went wrong here: %v", 123)

	// Fatal log - used to note that something in the process failed completely so the process couldnt finsih properly
	clogger.Fatal("something went seriously wrong here")
	clogger.Fatal("something went seriously wrong here: %v", 123)

	// Gracefully wait for integrations to finish threads
	time.Sleep(time.Second * 5)
}
