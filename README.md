# cLogger Package

Logger package that handles writing to files in production and console in development.

Logs with timestamps and stack traces to make it easier to track down errors when debugging.

## Usage

### Import

```go
import "gitlab.com/adon-inc/curbs/pkgs/clogger"
```

### Usage

```go
var env string = "development"
var filePath = "./"

// Initialize the logger
err := clogger.Init(env, filePath)
if err != nil {
    // something went wrong while setting up the logger
}
defer clogger.Close()

// Setup teams support
clogger.SetTeams(clogger.Groups{
        clogger.LOG: []string{"webhook"},
})

// Standard log - used for general logging and notes
clogger.Log("just wanted to log somthing at this point")
clogger.Log("just wanted to log somthing at this point: %v", 123)


// Warning log - used to denote if somthing didnt work right but didnt quite break anything
clogger.Warn("something isnt quite right here")
clogger.Warn("something isnt quite right here: %v", 123)

// Error log - used to note that something is wrong in the action
clogger.Error("something went wrong here")
clogger.Error("something went wrong here: %v", 123)

// Fatal log - used to note that something in the process failed completely so the process couldnt finsih properly
clogger.Fatal("something went seriously wrong here")
clogger.Fatal("something went seriously wrong here: %v", 123)
```

### Output
```go
// Log output
2020/06/26 14:31:40 something isnt quite right here:: 123

// Warning output
2020/06/26 14:31:40 (warn) Ssomething isnt quite right here: 123
 goroutine 6 [running]:
gitlab.com/adon-inc/curbs/pkgs/clogger.Warn(0x55f1e5, 0x2d, 0xc000049f58, 0x1, 0x1)
        /clogger/clogger.go:67 +0x85
gitlab.com/adon-inc/curbs/pkgs/clogger.TestLogging(0xc000114120)
        /clogger/clogger_test.go:16 +0x115
testing.tRunner(0xc000114120, 0x560cf0)
        /go/src/testing/testing.go:991 +0xe3
created by testing.(*T).Run
        /go/src/testing/testing.go:1042 +0x35e

// Error output
2020/06/26 14:31:41 (error) something went wrong here: 123
 goroutine 6 [running]:
gitlab.com/adon-inc/curbs/pkgs/clogger.Error(0x55d450, 0x22, 0xc000049f58, 0x1, 0x1)
        /clogger/clogger.go:84 +0x85
gitlab.com/adon-inc/curbs/pkgs/clogger.TestLogging(0xc000114120)
        /clogger/clogger_test.go:19 +0x16a
testing.tRunner(0xc000114120, 0x560cf0)
        /go/src/testing/testing.go:991 +0xe3
created by testing.(*T).Run
        /go/src/testing/testing.go:1042 +0x35e

// Fatal output
2020/06/26 14:31:41 (fatal) something went seriously wrong here: 123
 goroutine 6 [running]:
gitlab.com/adon-inc/curbs/pkgs/clogger.Fatal(0x55edd6, 0x2b, 0xc000049f58, 0x1, 0x1)
        /clogger/clogger.go:101 +0x85
gitlab.com/adon-inc/curbs/pkgs/clogger.TestLogging(0xc000114120)
        /clogger/clogger_test.go:22 +0x1bf
testing.tRunner(0xc000114120, 0x560cf0)
        /go/src/testing/testing.go:991 +0xe3
created by testing.(*T).Run
        /go/src/testing/testing.go:1042 +0x35e
```
