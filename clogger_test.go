package clogger

import (
	"testing"
)

func TestLogging(t *testing.T) {
	// Initialze the logger
	Init("development", "")
	defer Close()

	// Log a line
	Log("Just wanted to log this test: %v", "t1")

	// Warning
	Warn("Something isn't quite right, but its fine: %v", "t2")

	// Error
	Error("Something went wrong right now: %v", "t3")

	// Fatal
	Fatal("Something went horribly wrong right now: %v", "t4")
}
